class CartItemsController < ApplicationController
    def create
      cart_item = CartItem.new(
        qty: params[:qty],
        product_id: params[:product_id],
        cart_id: current_cart.id
        )
        cart_item.save
     redirect_to root_path
    end

  def destroy
    CartItem.find(params[:id]).destroy
    redirect_to root_path
  end
end
