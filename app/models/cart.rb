class Cart < ActiveRecord::Base
  belongs_to :products
  has_many :cart_items, dependent: :destroy
end 
